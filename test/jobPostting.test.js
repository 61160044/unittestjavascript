const assert = require('assert')
const { checkEnableTime } = require("../jobPosting");

describe('JobPostion', function () {

  it('shoud return false when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น', function () {
    // arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 30)
    const expectedResult = false;
    // act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // assert
    assert.strictEqual(actualResult, expectedResult)
  });

  it('shoud return false when เวลาสมัครอยู่หลังเวลาสิ้นสุด', function () {
    //arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 6)
    const expectedResult = false;
    // act
    const actualResult = checkEnableTime(startTime, endTime, today)
    //assert
    assert.strictEqual(actualResult, expectedResult)
  });

  it('shoud return true when เวลาสมัครอยู่ระหว่างเวลาเริ่มต้นและสิ้นสุด', function () {
    //arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 3)
    const expectedResult = true;
    //act
    const actualResult = checkEnableTime(startTime, endTime, today)
    //assert
    assert.strictEqual(actualResult, expectedResult)
  })

  it('shoud return true when เวลาสมัครเท่ากับเวลาเริ่มต้น', function () {
    //arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 31)
    const expectedResult = true;
    //act
    const actualResult = checkEnableTime(startTime, endTime, today)
    //assert
    assert.strictEqual(actualResult, expectedResult)
  })

  it('shoud return true when เวลาสมัครเท่ากับเวลาสิ้นสุด', function () {
    //arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 5)
    const expectedResult = true;
    //act
    const actualResult = checkEnableTime(startTime, endTime, today)
    //assert
    assert.strictEqual(actualResult, expectedResult)
  })

});
