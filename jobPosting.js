exports.checkEnableTime = function (stratTime, endTime, today) {

  if (today < stratTime) {
    return false
  }
  if (today > endTime) {
    return false
  }
  return true
}
